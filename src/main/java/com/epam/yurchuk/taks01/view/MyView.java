package com.epam.yurchuk.taks01.view;

import com.epam.yurchuk.taks01.annotation.MyField;
import com.epam.yurchuk.taks01.model.FirstModel;
import com.epam.yurchuk.taks01.model.MyGenericClass;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private Map<String, String> menu;
    private Map<String, PrintTable> methodsMenu;
    private static Scanner scanner = new Scanner(System.in);

    public MyView() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        menu.put("1", " 1 - Test field with annotation");
        menu.put("2", " 2 - Test unknown object");
        menu.put("3", " 3 - Test generic");
        menu.put("4", " 4 - Test invoke method, string args...");
        menu.put("Q", " Q - Exit");

        methodsMenu.put("1", this::test1);
        methodsMenu.put("2", this::test2);
        methodsMenu.put("3", this::test3);
        methodsMenu.put("4", this::test4);


    }

    private void test1() {
        System.out.println("1 - Test field with annotation");
        Class<FirstModel> clazz = FirstModel.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(MyField.class)) {
                MyField myField = (MyField) field.getAnnotation(MyField.class);
                String name = myField.name();
                int age = myField.age();
                System.out.println("field: " + field.getName());

                System.out.println("  name in @MyField:" + name);
                System.out.println("  age in @MyField: " + age);
            }
        }
    }

    private void test2() {
        System.out.println("2 - Test unknown object");
        FirstModel firstModel = new FirstModel();
        reflUnknownObject(firstModel);
    }

    private void reflUnknownObject(Object object) {
        try {
            Class clazz = object.getClass();
            System.out.println("The name of class is: " + clazz.getSimpleName());
            System.out.println("The name of class is: " + clazz.getName());

            System.out.println("Methods:");
            Method[] methods = clazz.getMethods();

            for (Method method : methods) {
                System.out.println("  " + method.getName());
            }

            System.out.println();

            System.out.println("The declareted fields of class: ");
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                System.out.println("  " + field.getName());
            }

            fields[0].setAccessible(true);
            if (fields[0].getType() == int.class) {
                fields[0].setInt(object, 22);
            }
            System.out.println(object);

            Method methodcall = clazz.getDeclaredMethod("getInfo");
            methodcall.setAccessible(true);

            methodcall.invoke(object);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void test3() throws InstantiationException, IllegalAccessException, NoSuchMethodException {
        System.out.println("3 - Test generic");
        MyGenericClass<FirstModel> obj = new MyGenericClass<>(FirstModel.class);
    }

    private void test4() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        System.out.println("4 - Test");
        FirstModel firstModel = new FirstModel();
        Class clazz = firstModel.getClass();
        String[] strs = {"one", "two", "three"};
        try {
            Method method =
                    clazz.getDeclaredMethod("methodString", new Class[]{String.class, String[].class});
            method.setAccessible(true);
            String[] str2 = (String[]) method.invoke(firstModel, "Hello", strs);
            for (String str : str2) {
                System.out.println(str);
            }
        } catch (NoSuchMethodException | SecurityException e) {
            e.printStackTrace();
        }
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

}
