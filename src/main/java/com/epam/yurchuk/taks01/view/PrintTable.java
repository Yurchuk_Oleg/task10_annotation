package com.epam.yurchuk.taks01.view;

import java.lang.reflect.InvocationTargetException;

public interface PrintTable {
    void print() throws IllegalAccessException, NoSuchMethodException, InstantiationException, InvocationTargetException;
}
