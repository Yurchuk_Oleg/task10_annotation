package com.epam.yurchuk.taks01.model;

import com.epam.yurchuk.taks01.annotation.MyField;

public class FirstModel {
    @MyField(name = "Test.txt")
    private int iValue;
    @MyField
    private String name;
    private double dValue;

    public int getiValue() {
        return iValue;
    }

    public void setiValue(int iValue) {
        this.iValue = iValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getdValue() {
        return dValue;
    }

    public void setdValue(double dValue) {
        this.dValue = dValue;
    }

    @Override
    public String toString() {
        return "FirstModel{" +
                "iValue=" + iValue +
                ", name='" + name + '\'' +
                ", dValue=" + dValue +
                '}';
    }

    private void getInfo() {
        System.out.println("Hello from getInfo method!");
    }

    public String[] methodString(String str, String... args) {
        String[] strings = new String[args.length];
        for (int i = 0; i < args.length; i++) {
            strings[i] = str + " ---> " + args[i] + " ===> " + i;
        }
        return strings;
    }
}
