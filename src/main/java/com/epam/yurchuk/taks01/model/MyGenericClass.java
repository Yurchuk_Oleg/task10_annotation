package com.epam.yurchuk.taks01.model;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class MyGenericClass<T> {
    private T obj;

    private final Class<T> clazz;

    public MyGenericClass(Class<T> clazz) throws IllegalAccessException, InstantiationException, NoSuchMethodException {
        this.clazz = clazz;
        try {
            obj = clazz.newInstance();
            obj = clazz.getConstructor().newInstance();

            System.out.println("The declareted fields: " + clazz.getSimpleName());
            System.out.printf(clazz.getName());

            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                System.out.println("  " + field.getName());
            }
            System.out.println();

        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
