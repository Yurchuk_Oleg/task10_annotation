package com.epam.yurchuk.taks01;

import com.epam.yurchuk.taks01.view.MyView;

public class Application {
    public static void main(String[] args) {
        new MyView().show();
    }
}
